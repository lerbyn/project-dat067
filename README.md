# Kubernetes Cost Estimation

Cost Estimation is a service which calculates and estimates the hourly prices of pods running on a kubernetes cluster by requesting metrics from kube-state-metrics, Metrics Server and Microsoft Azure Retail Prices API. For the service to work the kubernetes cluster has to be deployed on Azures kubernetes service(AKS) and have kube-state-metrics and Metrics Server installed.

The price metrics then get exposed on a http endpoint on the listening port (default 8080).

## Table of contents
* [Documentation](#documentation)
* [Cost Estimation metrics](#cost-estimation-metrics)
* [Getting Started](#getting-started)
* [Prerequisites](#prerequisites)
* [Installing](#installing)
* [Deployment](#deployment)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

## Documentation

The code documentaion in the form of JavaDoc is in the [JavaDoc directory.](https://gitlab.com/lerbyn/project-dat067/-/tree/master/JavaDoc)

## Cost Estimation metrics

Example of how the price metrics look like.

```
kube_pod_container_current_cost{namespace="namespace1",pod="pod1",container="container1",node="node1"} 0.008
kube_pod_container_requested_cost{namespace="namespace1",pod="pod1",container="container1",node="node1"} 0.012
kube_pod_container_current_cost{namespace="namespace1",pod="pod2",container="container2",node="node2"} 0.003
kube_pod_container_requested_cost{namespace="namespace1",pod="pod2",container="container2",node="node2"} 0.009
```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to have to install the service

```
docker
kubectl
```

What things you need to have to run the service

```
AKS Kubernetes cluster
kube-state-Metrics
Metrics Server
```

### Installing

How to build and install the project from the source code.

Download or copy the repository

```
git clone https://gitlab.com/lerbyn/project-dat067
```

**Building the image**

```
docker build
```

## Deployment

### Kubernetes

To deploy this project run ` kubectl apply -f /cost-estimation.yaml `, this creates a Kubernetes Deployment and service.

## Built With

* [Java](https://www.oracle.com/java/technologies/javase-downloads.html) - Programing Language 
* [Spring Boot](https://github.com/spring-guides/gs-spring-boot) - Application Building 
* [Gradle](https://gradle.org/) - Dependency Management
* [JSON-Java](https://github.com/stleary/JSON-java) - JSON processing
* [Docker](https://www.docker.com/) - Containerizing 

## Authors

* **David Anderson** - *Group Member* - [lerbyn](https://gitlab.com/lerbyn)
* **Matteus Straznyk** - *Group Member* - [Game9954](https://gitlab.com/Game9954)
* **Simon Terling** - *Group Member* - [SimonTer](https://gitlab.com/SimonTer)
* **Max Villing** - *Group Member* - [ChaoticVanguard](https://gitlab.com/ChaoticVanguard)
* **Philip Laine** - *Project Owner* - [phillebaba](https://gitlab.com/phillebaba)

See also the list of [contributors](https://gitlab.com/lerbyn/project-dat067/-/graphs/master) who participated in this project.

## License

none yet

## Acknowledgments

* Sakib, our Chalmers supervisor

