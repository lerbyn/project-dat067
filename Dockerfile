FROM gradle:jre15 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon
RUN ls ./build/libs
FROM openjdk:15-alpine
WORKDIR /app
COPY --from=build /home/gradle/src/build/libs/kostOpt-1.0.0.jar .
CMD ["java","-jar","/app/kostOpt-1.0.0.jar"]
