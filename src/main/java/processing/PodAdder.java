package processing;

import java.util.List;

/**
 * Interface for adding Pods to the podManager
 * @author Matteus Straznyk
 * @version 18-11-2020
 */
public interface PodAdder {

    void addCurrentPods(List<String> Fetchlist);

    void addRequestPods(List<String> Fetchlist);
}
