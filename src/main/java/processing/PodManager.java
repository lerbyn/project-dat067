package processing;

import input.Parser;
import org.json.JSONObject;

import java.util.*;

/**
 * Class that holds a map of all pods as well as operations that can be applied on the pods
 * @author Lerbyn, Matteus Straznyk
 */
public class PodManager implements PodGetter,PodAdder{

    private final HashMap<String,Pod> podMap;
    private final HashMap<String,Pod> latestPodMap;

    /**
     * Constructor for the pod manager class
     */
    public PodManager() {
        podMap = new HashMap<>();
        latestPodMap = new HashMap<>();
    }

    /**
     * Updates the podMap with new information held in latestPodMap
     * It removes pods that disappeared in the newest fetch
     * It updates the moving price window of the pods and updates their Moving Price Average
     */
    public void update() {
        //Remove "dead" pods
        Iterator<Map.Entry<String, Pod>> it = this.podMap.entrySet().iterator();
        while(it.hasNext()){
            String podName = it.next().getKey();
            if(!latestPodMap.containsKey(podName)) {
                it.remove();
            }
        }

        //update podMap with the new information
        for(String latestPod : this.latestPodMap.keySet()) {
            //System.out.println(latestPod);
            if(podMap.containsKey(latestPod) && podMap.get(latestPod).isFinishedInit() && podMap.get(latestPod).isAddedCurrent()) {
                podMap.get(latestPod).updatePriceWindow(latestPodMap.get(latestPod).getTempPriceRequest(),latestPodMap.get(latestPod).getTempPriceCurrent());
                //by using the pods priceWindow calculates the moving average with the constant MOVINGWINDOW as the window size
                podMap.get(latestPod).setMovingPriceAverageRequest(Calculator.movingAverage(podMap.get(latestPod).getPriceWindowRequest(),Constants.MOVINGWINDOW));
                podMap.get(latestPod).setMovingPriceAverageCurrent(Calculator.movingAverage(podMap.get(latestPod).getPriceWindowCurrent(),Constants.MOVINGWINDOW));
            } else {
                podMap.put(latestPod,latestPodMap.get(latestPod));
            }
        }
        latestPodMap.clear();
    }

    /**
     * Method create a pod all pod information but memory and add it to the temporary podmap "latestPodMap"
     * @param podName the name of the pod
     * @param namespaceName the name of the pods namespace
     * @param containerName the name of the container the pod is located in
     * @param nodeName the node on which the pod is located in
     * @param vmName the name of the Virtual Machine the is running on
     * @param cpu how many cpu cores the pod is currently utilizing
     */
    private void addPodCpu(String podName, String namespaceName,String containerName, String nodeName, String vmName,double cpu){
        if(!latestPodMap.containsKey(podName)) {

            // Checks if the vm name has an uppercase S in its name
            int index = vmName.lastIndexOf("S");
            if(index > 10){

                StringBuilder sb = new StringBuilder(vmName);
                sb.replace(index, index+1, "s");
                vmName = sb.toString();
            }

            latestPodMap.put(podName, new Pod(podName,namespaceName,containerName,nodeName,vmName,cpu));
        }
    }

    /**
     * Method to add memory information to an already created pod and making the pod "finished"
     * @param podName the name of the pod
     * @param memory how much memory in bytes the pod is currently utilizing
     */
    private void addPodMemory(String podName, double memory){
        if(latestPodMap.containsKey(podName)) {
            Pod pod = latestPodMap.get(podName);
            pod.finishInit(Calculator.calculatePrice(pod.getCpu(),memory,VMManager.getVM(pod.getNodeName(),pod.getVmName())));
        }
    }

    /**
     * Method to add all pods from a fetch the the temporary podmap "latestPodMap"
     * @param Fetchlist The list from fetch to be parsed
     */
    @Override
    public void addRequestPods(List<String> Fetchlist){ // O(podCpuRequestList + podMemoryRequestList + nodeLabelList)

        // Parses out pod cpu requests
        List<List<String>> podCpuList = Parser.readFile(Fetchlist, new String[]{"kube_pod_container_resource_requests_cpu_cores", "namespace","pod","container","node"});

        // Parses out pod memory requests
        List<List<String>> podMemoryList = Parser.readFile(Fetchlist, new String[]{"kube_pod_container_resource_requests_memory_bytes","pod"});

        // Parses out node labels
        List<List<String>> nodeLabelList = Parser.readFile(Fetchlist, new String[]{"kube_node_labels", "node","label_node_kubernetes_io_instance_type"});

        // Map of nodes and Virtual Machines
        HashMap<String,String> vmMap = new HashMap<>();


        for (List<String> l : nodeLabelList){ // Loops through the nodeLabelList to find the VM names and add it to the node map
            if(!(l.get(2).equals("") || l.get(4).equals("") || l.get(2) == null || l.get(4) == null)) {
                vmMap.put(l.get(2), l.get(4));
            }
        }

        for (List<String> i : podCpuList){ //Loops through the podCpuRequestList
            if(!(i.get(2).equals("") || i.get(4).equals("") || i.get(6).equals("") || i.get(8).equals("") || i.get(9).equals("") || i.get(2) == null || i.get(4) == null || i.get(6) == null || i.get(8) == null|| i.get(9) == null)) {
                addPodCpu(i.get(4), i.get(2), i.get(6), i.get(8), vmMap.get(i.get(8)), Double.parseDouble(i.get(9))); // Then adds the pod with the information it has found
            }
        }

        for (List<String> j : podMemoryList){ // Loops through the podMemoryRequestList to find the memory
            if(!(j.get(2).equals("") || j.get(3).equals("") || j.get(2) == null || j.get(3) == null)) {
                addPodMemory(j.get(2), Double.parseDouble(j.get(3))); // Then adds the information it has found to the pod, if it exists
            }
        }
    }

    /**
     * Method to add all pods from a fetch the the temporary podmap "latestPodMap"
     * @param Fetchlist The list from fetch to be parsed
     */
    @Override
    public void addCurrentPods(List<String> Fetchlist){ // O(podCpuRequestList + podMemoryRequestList + nodeLabelList)

        if(Fetchlist.isEmpty()){ //if nothing is fetched return
            return;
        }

        // Converts the received json data to the internal json class
        final JSONObject jsonRepresentation = new JSONObject(Fetchlist.get(0));

        for(int i = 0; i < jsonRepresentation.getJSONArray("items").length();i++){

            String name = jsonRepresentation.getJSONArray("items").getJSONObject(i).getJSONObject("metadata").getString("name");
            String cpuString = jsonRepresentation.getJSONArray("items").getJSONObject(i).getJSONArray("containers").getJSONObject(0).getJSONObject("usage").getString("cpu");
            String memoryString = jsonRepresentation.getJSONArray("items").getJSONObject(i).getJSONArray("containers").getJSONObject(0).getJSONObject("usage").getString("memory");

            double cpu = 0;
            double memory = 0;

            // removes prefix and unit
            int index = cpuString.indexOf("n");
            if(index != -1){

                StringBuilder sb = new StringBuilder(cpuString);
                sb.deleteCharAt(index);
                cpu = Double.parseDouble(sb.toString());
                cpu = cpu * 0.000000001d;
            }

            // removes prefix and unit
            int index2 = memoryString.indexOf("Ki");
            if(index2 != -1){

                StringBuilder sb = new StringBuilder(memoryString);
                sb.delete(index2, index2+2);
                memory = Double.parseDouble(sb.toString());
                memory = memory * 1000d;
            }

            if(latestPodMap.containsKey(name)){
                latestPodMap.get(name).setTempPriceCurrent(Calculator.calculatePrice(cpu,memory,VMManager.getVM(latestPodMap.get(name).getNodeName(),latestPodMap.get(name).getVmName())));
                latestPodMap.get(name).setAddedCurrent(true);
            }

        }
    }

    public void addTestPod(String podName, String namespaceName,String containerName, String nodeName, String vmName,double cpu,double memory) {
        addPodCpu(podName, namespaceName,containerName,nodeName,vmName,cpu);
        addPodMemory(podName,memory);
        latestPodMap.get(podName).setTempPriceCurrent(Calculator.calculatePrice(cpu,memory,VMManager.getVM(latestPodMap.get(podName).getNodeName(),latestPodMap.get(podName).getVmName())));
        latestPodMap.get(podName).setAddedCurrent(true);
    }


    /**
     * Getter for the Pod List
     * @return the Pod List
     */
    @Override
    public List<Pod> getPodList(){
        return new ArrayList<>(podMap.values());
    }
}
