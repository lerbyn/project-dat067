package processing;

import input.VMFetcher;

import java.util.HashMap;

/**
 * Class for managing Virtual Machines
 * @author Matteus Straznyk, Max Villing
 * @version 07-12-2020
 */
public class VMManager {

    private static final HashMap<String,VirtualMachine> vmmap = new HashMap<>();
    
    /**
     * Get the Virtual Machine for the node
     * @param nodeName the name of the node info to
     * @param vmName the name of the node's virtual machine
     * @return the Virtual Machine requested
     */
    public static VirtualMachine getVM(String nodeName, String vmName){

        if(!vmmap.containsKey(vmName)){
            vmmap.put(vmName, VMFetcher.getVM(nodeName,vmName));
        }
        return vmmap.get(vmName);
    }

    /**
     * Updates the prices for all the virtual machines in memory
     */
    public void updateVirtualMachinePrices(){
        for(VirtualMachine vm: vmmap.values()){
            vm.setPrice(VMFetcher.getVMPrices(vm));
        }
    }

}
