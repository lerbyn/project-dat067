package processing;

/**
 * Class for Virtual Machines
 * @author Matteus Straznyk
 * @version 08-12-2020
 */
public class VirtualMachine {

    private final String name;
    private final String region;
    private final double cpus;
    private final double memory;
    private double price;

    /**
     * Constructor for the VirtualMachine class
     * @param name of the VirtualMachine
     * @param region of the VirtualMachine
     * @param cpus of the VirtualMachine
     * @param memory of the VirtualMachine
     * @param price of the VirtualMachine
     */
    public VirtualMachine(String name, String region, double cpus, double memory, double price) {
        this.name = name;
        this.region = region;
        this.cpus = cpus;
        this.memory = memory;
        this.price = price;
    }

    /**
     * Getter for Name
     * @return Name of the VirtualMachine
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for Cpus
     * @return Cpus of the VirtualMachine
     */
    public double getCpus() {
        return cpus;
    }

    /**
     * Getter for Memmory
     * @return Memmory of the VirtualMachine
     */
    public double getMemory() {
        return memory;
    }

    /**
     * Getter for Price
     * @return Price of the VirtualMachine
     */
    public double getPrice() {
        return price;
    }

    /**
     * Getter for VirtualMachine region
     * @return region of the VirtualMachine
     */
    public String getRegion() {
        return region;
    }

    /**
     * Setter for Price
     * @param price the new price for the virtual machine
     */
    public void setPrice(double price) {
        this.price = price;
    }
}
