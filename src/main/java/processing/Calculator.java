package processing;

import java.util.List;

/**
 * class to handle calculations of data
 * @author Lerbyn
 */
public class Calculator {

    /**
     * @param data list of data points
     * @param movingWindow how large the window should be where the moving average is calculated
     * @return the average of the provided data points
     */
    public static double movingAverage(List<Double> data, int movingWindow) {
        if(data.size() == movingWindow) {
            double result = 0;
            for(double d : data) {
                result += d;
            }
            result = (result/data.size());
            return (Math.round(result*1000)/1000.0);//rounds to 3 decimals
        }
        return -1;
    }

    /**
     * calculates the current price of an application
     * @param cpu the amount of cpu cores the application is using
     * @param memory the amount of memory in gb the application is using
     * @param vm what type of virtualMachine the application is running on
     * @return the current price of the application
     */
    public static double calculatePrice(double cpu, double memory, VirtualMachine vm) {
        double cpuUtil = cpu/vm.getCpus();
        double memUtil = memory/vm.getMemory();
        return (cpuUtil*0.5 + memUtil*0.5) * vm.getPrice();
    }
}
