package processing;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to represent a Pod from kube state metrics
 * @author Matteus Straznyk
 * @version 18-11-2020
 */
public class Pod {

    private final String podName;
    private final String namespaceName;
    private final String containerName;
    private final String nodeName;
    private final String vmName;
    private final double cpu;

    private boolean finishedInit;
    private boolean addedCurrent;

    private final List<Double> priceWindowRequest;
    private double movingPriceAverageRequest;
    private double tempPriceRequest;

    private final List<Double> priceWindowCurrent;
    private double movingPriceAverageCurrent;
    private double tempPriceCurrent;

    /**
     * Constructor for the Temporary Pods
     * @param podName The name of the Pod added
     * @param namespaceName The name of the namespace of the Pod
     * @param containerName The name of the Container of the Pod
     * @param nodeName The name of the Node the Pod is running on
     * @param vmName The name of the Virtual Machine the Pod is running on
     * @param cpu The amount of cpu millicores the pod uses
     */
    public Pod(String podName, String namespaceName, String containerName, String nodeName, String vmName, Double cpu) {
        this.podName = podName;
        this.namespaceName = namespaceName;
        this.containerName = containerName;
        this.nodeName = nodeName;
        this.vmName = vmName;
        this.finishedInit = false;
        this.addedCurrent = false;
        this.cpu = cpu;

        this.priceWindowRequest = new ArrayList<>();
        this.tempPriceRequest = -1;
        this.movingPriceAverageRequest = -1;

        this.priceWindowCurrent = new ArrayList<>();
        this.movingPriceAverageCurrent = -1;
        this.tempPriceCurrent = -1;
    }

    /**
     * Updates the priceWindow list with a new currentPrice
     * If the priceWindow is already full, it removes the oldest value and adds the new currentPrice
     * @param currentPrice The Price to be added to the priceWindow
     */
    public void updatePriceWindow(double requestedPrice,double currentPrice){
        if(priceWindowRequest.size() >= Constants.MOVINGWINDOW){
            priceWindowRequest.remove(0);

        }
        priceWindowRequest.add(requestedPrice);

        if(priceWindowCurrent.size() >= Constants.MOVINGWINDOW){
            priceWindowCurrent.remove(0);
        }
        priceWindowCurrent.add(currentPrice);
    }

    /**
     * Setter for the MovingPriceAverage
     * @param price The new moving price average
     */
    public void setMovingPriceAverageRequest(double price){
        this.movingPriceAverageRequest = price;
    }

    public void setMovingPriceAverageCurrent(double price) {
        this.movingPriceAverageCurrent = price;
    }

    public void setTempPriceCurrent(double tempPriceCurrent) {
        this.tempPriceCurrent = tempPriceCurrent;
    }

    public void setAddedCurrent(boolean addedCurrent) {
        this.addedCurrent = addedCurrent;
    }

    public void finishInit(double tempPrice) {
        this.tempPriceRequest = tempPrice;
        this.finishedInit = true;
    }

    /**
     * Getter for the priceWindow
     * @return the priceWindow
     */
    public List<Double> getPriceWindowRequest() {
        return priceWindowRequest;
    }

    /**
     * Getter for the Temporary Price
     * @return the temporary price
     */
    public double getTempPriceRequest() {
        return tempPriceRequest;
    }

    /**
     * Getter for the Pod name
     * @return the Pod name
     */
    public String getPodName() {
        return podName;
    }

    /**
     * Getter for the name of the Contatiner the Pod is in
     * @return the Contatiner name
     */
    public String getContainerName() {
        return containerName;
    }

    /**
     * Getter for the name of the Node the Pod is in
     * @return the Node name
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * Getter for the name of the VirtualMachine the Pod is in
     * @return the VirtualMachine name
     */
    public String getVmName() {return vmName;}

    /**
     * Getter for the amount of cpu millicores the pos uses
     * @return the amount of cpu millicores the pos uses
     */
    public double getCpu() {
        return cpu;
    }

    /**
     * Getter for the name of the Namespace the Pod is in
     * @return the Namespace name
     */
    public String getNamespaceName() {
        return namespaceName;
    }

    /**
     * Getter for the finishedInit boolean flag, it represents if the pod has been fully constructed
     * @return the finishedInit boolean flag
     */
    public boolean isFinishedInit() {
        return finishedInit;
    }

    /**
     * Getter for the Pods moving price average
     * @return the MovingPriceAverage
     */
    public double getMovingPriceAverageRequest() {
        return movingPriceAverageRequest;
    }

    public List<Double> getPriceWindowCurrent() {
        return priceWindowCurrent;
    }

    public double getMovingPriceAverageCurrent() {
        return movingPriceAverageCurrent;
    }

    public double getTempPriceCurrent() {
        return tempPriceCurrent;
    }

    public boolean isAddedCurrent() {
        return addedCurrent;
    }

    /**
     * Equals method for the pod class
     * @return true if the objects are the same; false otherwise.
     */
    @Override
    public boolean equals(Object pod) {
        if(pod instanceof Pod) {
            Pod p = (Pod) pod;
            return this.podName.equals(p.getPodName()) && this.containerName.equals(p.getContainerName()) && this.nodeName.equals(p.getNodeName()) && this.vmName.equals(p.getVmName()) && this.priceWindowRequest.equals(getPriceWindowRequest()) && this.tempPriceRequest == p.getTempPriceRequest() && this.movingPriceAverageRequest == p.getMovingPriceAverageRequest();
        }
        return false;
    }

    public boolean equalsBeforeCalculations(Pod p) {
        return this.podName.equals(p.getPodName()) && this.containerName.equals(p.getContainerName()) && this.nodeName.equals(p.getNodeName()) && this.vmName.equals(p.getVmName());
    }
}
