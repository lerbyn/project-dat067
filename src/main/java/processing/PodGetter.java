package processing;

import java.util.List;

/**
 * Interface for getting the Pod List from the PodManager
 * @author Matteus Straznyk
 * @version 18-11-2020
 */
public interface PodGetter {

    /**
     * Getter for the Pod List
     * @return the Pod List
     */
    List<Pod> getPodList();
}
