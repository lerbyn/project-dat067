package output;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import processing.Pod;
import processing.PodGetter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

/**
 * Class that hosts a http server that exposes pod information via http get requests
 * @author Matteus Straznyk
 * @version 23-11-2020
 */
public class Exposer {

    private static PodGetter podGetter;

    public Exposer(PodGetter podGetter) {
        Exposer.podGetter = podGetter;


        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
            HttpContext context = server.createContext("/");
            context.setHandler(Exposer::handleRequest);
            server.start();
        }catch (IOException e) {
            System.exit(-1);
        }
    }

    private static void handleRequest(HttpExchange httpExchange){

        try {
            StringBuilder str = new StringBuilder();
            for (Pod pod : podGetter.getPodList()) {
                if(pod.getMovingPriceAverageRequest() != -1) { // -1 represents the moving window not being filled, meaning we don't want to expose MovingPriceAverage yet
                    str.append("kube_pod_container_requested_cost{namespace=\"");
                    str.append(pod.getNamespaceName());
                    str.append("\",pod=\"");
                    str.append(pod.getPodName());
                    str.append("\",container=\"");
                    str.append(pod.getContainerName());
                    str.append("\",node=\"");
                    str.append(pod.getNodeName());
                    str.append("\"} ");
                    str.append(pod.getMovingPriceAverageRequest());
                    str.append("\n");
                }
                if(pod.getMovingPriceAverageRequest() != -1) {
                    str.append("kube_pod_container_current_cost{namespace=\"");
                    str.append(pod.getNamespaceName());
                    str.append("\",pod=\"");
                    str.append(pod.getPodName());
                    str.append("\",container=\"");
                    str.append(pod.getContainerName());
                    str.append("\",node=\"");
                    str.append(pod.getNodeName());
                    str.append("\"} ");
                    str.append(pod.getMovingPriceAverageCurrent());
                    str.append("\n");
                }
            }

            String response = str.toString();

            httpExchange.sendResponseHeaders(200, response.getBytes().length);//response code and length
            try(OutputStream os = httpExchange.getResponseBody()) {
                os.write(response.getBytes());
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
