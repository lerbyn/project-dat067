package input;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * class to fetch data from outside sources
 * @author Lerbyn
 */
public class Fetch {

    /**
     * @param url the url you wanna send a get request to
     * @return the data received from the get request
     * @throws IOException if the url cant be reached
     */
    public static List<String> getHTTP(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            List<String> result = new LinkedList<>();
            String line;
            while ((line = br.readLine()) != null) {
                oneLineFormatSplitt(result, line);
            }
            br.close();
            System.out.println("successfully did HTTP GET on " + url);
            return result;
        }catch( Exception e) {
            System.out.println("http fail: " + e);
        }
        return null;
    }

    /**
     * @return the data received from the get request
     * @throws IOException if the url cant be reached
     * @throws NoSuchAlgorithmException if the url cant be reached
     * @throws KeyManagementException if the url cant be reached
     */
    public static List<String> getHTTPS(URL url, String token) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        //disable tls
        javax.net.ssl.SSLContext context = javax.net.ssl.SSLContext.getInstance("TLS");
        TrustManager veryTrusting = new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) {
            }
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
        context.init(null, new TrustManager[]{veryTrusting}, null);
        connection.setRequestMethod("GET");
        //add bearer token to request
        if(!token.equals("")){
            connection.setRequestProperty("Authorization","Bearer " + token);
        }
        connection.setSSLSocketFactory(context.getSocketFactory());
        try(BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            List<String> result = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                oneLineFormatSplitt(result, line);
            }
            br.close();
            System.out.println("successfully did HTTPS GET on " + url);
            return result;
        } catch( Exception e) {
            System.out.println("https fail: " + e);
        }
        return null;
    }

    public static List<String> testFetcher(String fileName) throws IOException{
        File file = new File("configFile", fileName);
        if(file.exists()){
            try (BufferedReader frd = new BufferedReader(new FileReader(file))) {
                String line;
                List<String> metricsData = new ArrayList<>();
                while ((line = frd.readLine()) != null) {
                    oneLineFormatSplitt(metricsData, line);

                }
                return metricsData;
            }
        }else {
            System.exit(-2);
        }
        return null;
    }

    private static void oneLineFormatSplitt(List<String> list, String line){
        String[] splitLine = line.split("(, )");
        list.addAll(Arrays.asList(splitLine));
    }

}
