package input;

import java.io.File;
import java.util.*;

/**
 * The type Parser.
 */
public class Parser {

    private File file;

    /**
     * Instantiates a new Parser.
     */
    public Parser() {
        //null constructor
    }

    /**
     * Parsing the data to a more userfriednly format.
     * @param metricsData A List with the Prometheus-data
     * @param tag An array with the tags to be used. The first element in tag is the "line-tag".
     *           All lines that doesn't match will it be ignored.
     *           All other elements after that is "meta-data-tag's".
     *           If no "meta-data-tags" is provided, the data won't be filtered.
     * @return Returns a List within a list with parsed data. Each list element contains
     * filtered information (tag)
     */
    public static List<List<String>> readFile(List<String> metricsData, String[] tag) {
        List<List<String>> nodeData = new ArrayList<>();
        List<String> tempNodeData; //Holds the data returned from lexer before it's stored in nodeData
        //add the data to the List if it isn't null
        for (String line : metricsData){
            tempNodeData = lexer(line, tag);
            if(tempNodeData != null){
                nodeData.add(tempNodeData);
            }
        }

        //printNodeData(nodeData);
        //printNodeData(nodeData);
        return nodeData;
    }

    /**
     * Split the String whenever a delimiter occurs in the text
     * @param indata a String in Prometheus-metrisc format
     * @param tag a tag containing what information is relevant
     * @return a String-array. The first element is the "line-tag". After that every other element is a "meta-data-tag"
     * followed by the tag-information. {"tag", "metatag", "taginfo", "metatag2", "taginfo2"}
     */
    private static List<String> lexer (String indata, String[] tag){
        List<String> d = new ArrayList<>();
        String[] temp = indata.split("(=\")|(\"\")|(\",)|(\"})|[{,]");

        //if the array has the correct tag, split and trim the string
        if(temp.length > 0 && temp[0].equals(tag[0])) {
            for (int i = 0; i < temp.length; i++) {
                    d.add(temp[i].trim());
            }
        }
        //if the string doesn't contain the tag, return null
        else{
            return null;
        }
        //if metadata tags is provided, pick out the data from the tags
        if(tag.length > 1) {
           d = metaFilter(d, tag);
        }
        return d;
    }

    /**
     * Filters data
     * @param indata The parsed data of one node
     * @param tag An array containing information about what to filter
     * @return Filtered indata
     */
    private static List<String> metaFilter (List<String> indata, String[] tag){
        List<String> result = new ArrayList<>();
        result.add(indata.get(0)); //add the tag
        // add metadata with correct tag
        for(int i = 1; i < indata.size() - 1; i +=2){
            for (int j = 1; j < tag.length; j++) {
                if(indata.get(i).equals(tag[j])){
                    result.add(indata.get(i));
                    result.add(indata.get(i+1));
                }
            }
        }
        result.add(indata.get(indata.size()-1)); //adds the number after the curly bracket
        return result;
    }

    //debugging purpose only
    private static void printNodeData(List<List<String>> nodeData) {
        for (List<String> i : nodeData){ //Print data
            System.out.print("NEW POD DATA ");
            int index = 0;
            for (String j : i) {
                System.out.println("AT INDEX " + index + ": ");
                System.out.println(j);
                index++;
            }
        }
    }
}