package input;

import input.Fetch;
import input.Parser;
import org.json.JSONObject;
import processing.VirtualMachine;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import static processing.Constants.PROMETHEUSURL;

/**
 * Class for fetching Virtual Machine info from Prometheus and Microsoft Azure Retail Prices API
 * API Documentation: https://docs.microsoft.com/en-us/rest/api/cost-management/retail-prices/azure-retail-prices
 * @author Matteus Straznyk, Max Villing
 * @version 07-12-2020
 */
public class VMFetcher {

    /**
     * Returns a virtual machine with data from Microsoft Azure Retail Prices API and node data from Prometheus
     * @param nodeName the name of the node
     * @param vmName the name of the virtual machine
     * @return an instance of the VirtualMachine requested
     */
    public static VirtualMachine getVM(String nodeName, String vmName){

        VirtualMachine tempVM = new VirtualMachine("Standard_B2s","westeurope",2,4,1d);

        try {
            // Fetches
            List<String> list = Fetch.getHTTP(new URL(PROMETHEUSURL));
            if(list == null) {
                System.out.println("failed to fetch VM infromation prometheus kube-state-metrics");
                System.out.println("Fixed values will be used for VM prices based of the Standard B2s machine located in west Europe");
                return tempVM;
            }
            // Parses out node memory
            double memory = getNodeMemory(nodeName, list);

            // Parses out node cpu
            double cpus = getNodeCpus(nodeName, list);

            // Parses out node region
            String region = getNodeRegion(nodeName, list);

            // Gets prices from azure api
            String receivedData = getHTTPS(new URL("https://prices.azure.com/api/retail/prices?$filter=armSkuName%20eq%20%27" + vmName +"%27%20and%20armRegionName%20eq%20%27" + region +"%27%20and%20reservationTerm%20eq%20null"));
            if(receivedData == null) {
                System.out.println("failed to reach Microsft Azure api");
                System.out.println("Fixed values will be used for VM prices based of the Standard B2s machine located in west Europe");
                return tempVM;
            }
            // Converts the received json data to the internal json class
            final JSONObject jsonRepresentation = new JSONObject(receivedData);
            double price = jsonRepresentation.getJSONArray("Items").getJSONObject(0).getDouble("retailPrice");

            tempVM = new VirtualMachine(vmName, region, cpus, memory, price);

        } catch (IOException e) {
            System.out.println("Failed to retrieve VM information due to: " + e);
            System.out.println("Fixed values will be used for VM prices based of the Standard B2s machine located in west Europe");
        }
        return tempVM;
    }

    /**
     * Returns the region that the node's virtual machine is running in
     * @param nodeName the name of the node
     * @param list the list that the method parses and gets the node region
     * @return the region that the node's virtual machine is running in
     */
    private static String getNodeRegion(String nodeName, List<String> list) {
        // Parses out node region
        List<List<String>> regionList = Parser.readFile(list, new String[]{"kube_node_labels","node", "label_failure_domain_beta_kubernetes_io_region"});
        String region = "westeurope"; // Standard region is "westeurope" if it can't parse out a region

        // loops through all the kube_node_labels that the parser gives
        for (List<String> i : regionList){
            if(i.get(2).equals(nodeName)){ // Checks if the node name provided is in the parsed list
                region = i.get(4); // gets the region from list
            }
        }
        return region;
    }

    /**
     * Returns the amount of cpu cores that the node's virtual machine has
     * @param nodeName the name of the node
     * @param list the list that the method parses and gets the node region
     * @return the amount of cpu cores that the node's virtual machine has
     */
    private static double getNodeCpus(String nodeName, List<String> list) {
        // Parses out cpu
        List<List<String>> cpuList = Parser.readFile(list, new String[]{"kube_node_status_capacity_cpu_cores", "node"});
        double cpus = 0;

        // loops through all the kube_node_status_capacity_cpu_cores that the parser gives
        for (List<String> i : cpuList){
            if(i.get(2).equals(nodeName)){ // Checks if the node name provided is in the parsed lis
                cpus = Double.parseDouble(i.get(3)); // gets the cpus from list
            }
        }
        return cpus;
    }

    /**
     * Returns the amount of memory bytes that the node's virtual machine has
     * @param nodeName the name of the node
     * @param list the list that the method parses and gets the node region
     * @return the amount of memory bytes that the node's virtual machine has
     */
    private static double getNodeMemory(String nodeName, List<String> list) {
        // Parses out memory
        List<List<String>> memoryList = Parser.readFile(list, new String[]{"kube_node_status_capacity_memory_bytes", "node"});
        double memory = 0;

        // loops through all the kube_node_status_capacity_memory_bytes that the parser gives
        for (List<String> i : memoryList){
            if(i.get(2).equals(nodeName)){ // Checks if the node name provided is in the parsed lis
                memory = Double.parseDouble(i.get(3)); // gets the memory from list
            }
        }
        return memory;
    }

    /**
     * Returns the hourly price of the virtual machine
     * @param vm the name of the virtual machine to get the price from
     * @return the hourly price of the virtual machine
     */
    public static double getVMPrices(VirtualMachine vm){
        try {
            String receivedData = getHTTPS(new URL("https://prices.azure.com/api/retail/prices?$filter=armSkuName%20eq%20%27" + vm.getName() +"%27%20and%20armRegionName%20eq%20%27" + vm.getRegion() +"%27%20and%20reservationTerm%20eq%20null"));
            final JSONObject jsonRepresentation = new JSONObject(receivedData);
            return jsonRepresentation.getJSONArray("Items").getJSONObject(0).getDouble("retailPrice");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Does a HTTPS get request with the Content-Type header "application/json" to the provided url
     * @param url the url to do a HTTPS get from
     * @return the response from the HTTPS get request
     */
    public static String getHTTPS(URL url) throws IOException  {

        StringBuilder str = new StringBuilder();
        try {
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            try(BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                String line;
                while ((line = rd.readLine()) != null) {
                    str.append(line);
                }
            }

        } catch (IOException e) {
            System.out.println("HTTP get failed to retrieve vm information " + e);
        }
        return str.toString();
    }
}
