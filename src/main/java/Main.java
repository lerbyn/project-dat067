import input.Fetch;
import output.Exposer;
import processing.PodManager;
import processing.VMManager;
import java.net.URL;
import static java.lang.Thread.sleep;
import static processing.Constants.*;

/**
 * Main class of the application
 * It initializes all the managers and exposer
 * It then fetches pod information from prometheus with an interval of the constant FETCHTIMER seconds
 * It then gives the fetched information to the podManager
 * @author Matteus Straznyk
 * @version 08-12-2020
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        PodManager podManager = new PodManager();

        new VMManager();
        new Exposer(podManager);

        while(true) {
            try {

                podManager.addRequestPods(Fetch.getHTTP(new URL(PROMETHEUSURL)));
                podManager.addCurrentPods(Fetch.getHTTPS(new URL(METRICSSERVERURL),METRICSERVERTOKEN));

                podManager.update();

                System.out.println("Running");

            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                sleep(FETCHTIMER * 1000);
            }
        }
    }
}
