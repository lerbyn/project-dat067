package testing;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import processing.Pod;
import processing.PodManager;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class PodManagerTest {

    PodManager podManager;

    @Before
    public void setUp() throws Exception {
        podManager = new PodManager();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void podManagerUpdateTest1() { //add three pods then update add two of the same pods and update one pod should be removed

        podManager.addTestPod("1", "c", "n", "test", "Standard_B2s", 1, 8);
        podManager.addTestPod("2", "c", "n", "test", "Standard_B2s", 1, 8);
        podManager.update();
        podManager.addTestPod("2", "c", "n", "test", "Standard_B2s", 1, 8);
        podManager.update();
        List<Pod> podList = podManager.getPodList();
        Pod compPod = new Pod("2", "c", "n", "test", "Standard_B2s", 1.0);
        Pod compPodDeleted = new Pod("1", "c", "n", "test", "Standard_B2s", 1.0);
        assertEquals(1,podList.size());
        assertTrue(podList.get(0).equalsBeforeCalculations(compPod));
        assertFalse(podList.get(0).equalsBeforeCalculations(compPodDeleted));
    }

}