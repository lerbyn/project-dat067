package testing;

import org.junit.runner.*;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorTest.class, PodManagerTest.class, PodTest.class, VMFetcherTest.class, ParserTest.class})
public class TestAll{

}
