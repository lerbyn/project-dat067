package testing;


//import org.junit.jupiter.api.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;

import processing.Calculator;
import processing.VirtualMachine;



import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the class Calculator
 * @author Lerbyn
 */
public class CalculatorTest{


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    public void Test(){

    }

    @Test
    public void calculator_movingAverage_test1() { //tests if input.size() < movingWindow movingAverage should return -1
        List<Double> input = new ArrayList<>();
        input.add(1.0);
        input.add(2.0);
        Assertions.assertEquals(-1, Calculator.movingAverage(input,10));
    }

    @Test
    public void calculator_movingAverage_test2() { //tests if input.size() == movingWindow movingAverage should return -1
        List<Double> input = new ArrayList<>();
        input.add(1.0);
        input.add(2.0);
        assertEquals(1.5, Calculator.movingAverage(input,2));
    }

    @Test
    public void calculator_movingAverage_test3() { //tests if input.size() == movingWindow movingAverage should return -1
        List<Double> input = new ArrayList<>();
        input.add(1.0);
        input.add(2.0);
        assertEquals(-1, Calculator.movingAverage(input,1));
    }

    @Test
    public void calculator_movingAverage_test4() { //test if the method movingAverage returns 3 decimals
        List<Double> l = new ArrayList<>();
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.0);
        l.add(0.1111111111);
        assertEquals(0.011,Calculator.movingAverage(l,10));
    }

    @Test
    public void calculator_calculatePrice_test1() { //tests if it costs 100% of the price at 100% utilization
        assertEquals(10,Calculator.calculatePrice(4,32,new VirtualMachine("test","westeurope",4,32,10)));
    }

    @Test
    public void calculator_calculatePrice_test2() { //tests if it costs 75% of the price at 50% cpu utilization and 100% memory utilization
        assertEquals(7.5,Calculator.calculatePrice(2,32,new VirtualMachine("test","westeurope",4,32,10)));
    }

    @Test
    public void calculator_calculatePrice_test3() { //tests if it costs 50% of the price at 50% memory utilization and 100% cpu utilization
        assertEquals(7.5,Calculator.calculatePrice(4,16,new VirtualMachine("test","westeurope",4,32,10)));
    }

    @Test
    public void calculator_calculatePrice_test4() { //tests if it costs 50% of the price at 50% memory utilization and 50% cpu utilization
        assertEquals(5,Calculator.calculatePrice(2,16,new VirtualMachine("test","westeurope",4,32,10)));
    }

    @Test
    public void calculator_calculatePrice_test5() { //tests if it costs 0% of the price at 0% memory utilization and 0% cpu utilization
        assertEquals(0,Calculator.calculatePrice(0,0,new VirtualMachine("test","westeurope",4,32,10)));
    }


}