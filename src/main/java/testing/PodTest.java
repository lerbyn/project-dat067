package testing;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import processing.Pod;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PodTest {

    Pod pod;

    @Before
    public void setUp() {
        pod = new Pod("name","namespace", "container", "node", "vm", 2d);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void updatePriceWindow() {
        assertEquals(0, pod.getPriceWindowRequest().size());
        assertEquals(0, pod.getPriceWindowCurrent().size());
        pod.updatePriceWindow(3d, 3d);
        assertEquals(1, pod.getPriceWindowRequest().size());
        assertEquals(1, pod.getPriceWindowCurrent().size());
    }

    @Test
    public void setMovingPriceAverageRequest() {
        pod.setMovingPriceAverageRequest(3d);
        assertEquals(3d, pod.getMovingPriceAverageRequest());
    }

    @Test
    public void setMovingPriceAverageCurrent() {
        pod.setMovingPriceAverageCurrent(3d);
        assertEquals(3d, pod.getMovingPriceAverageCurrent());
    }

    @Test
    public void getTempPriceRequest() {
        assertEquals(-1d, pod.getTempPriceRequest());
    }

    @Test
    public void getTempPriceCurrent() {
        assertEquals(-1d, pod.getTempPriceCurrent());
    }


    @Test
    public void getPodName() {
        assertEquals("name", pod.getPodName());
    }

    @Test
    public void getContainerName() {
        assertEquals("container", pod.getContainerName());
    }

    @Test
    public void getNodeName() {
        assertEquals("node", pod.getNodeName());
    }

    @Test
    public void getMovingPriceAverageRequest() {
        assertEquals(-1, pod.getMovingPriceAverageRequest());
        pod.setMovingPriceAverageRequest(4d);
        assertEquals(4d, pod.getMovingPriceAverageRequest());
    }

    @Test
    public void getMovingPriceAverageCurrent() {
        assertEquals(-1, pod.getMovingPriceAverageCurrent());
        pod.setMovingPriceAverageCurrent(4d);
        assertEquals(4d, pod.getMovingPriceAverageCurrent());
    }
}