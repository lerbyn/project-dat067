package testing;

import input.Fetch;
import input.Parser;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParserTest {

    public List<String> inputData = null;
    @Before
    public void setUp() {

        try{
            inputData = Fetch.testFetcher("kostopt_prometheus.log");
        } catch (IOException e){
            e.printStackTrace();
        }
    }


    //Tom metadata efter tag utan filter
    //Tom metadata efter tag med filer
    //Hämta data med filter
    //Hämta data utan filter

    //Testing parser (lexer) with no filter
    @Test
    public void parseDataNF() {
        String[] tag = {"kube_endpoint_info"};
        List<List<String>> result =  Parser.readFile(inputData, tag);

        assertEquals("kube-system", result.get(10).get(2));
        assertEquals("kube-controller-manager", result.get(10).get(4));
        assertEquals("1", result.get(10).get(5));
    }

    //Testing parser (lexer) with filter
    @Test
    public void parseDataWF() {
        String[] tag = {"kube_endpoint_info", "endpoint", "namespace"};
        List<List<String>> result =  Parser.readFile(inputData, tag);

        assertEquals("kube-system", result.get(10).get(2));
        assertEquals("kube-controller-manager", result.get(10).get(4));
        assertEquals("1", result.get(10).get(5));
    }

    //Tests if the parser works correctly when metadata is empty with filter
    @Test
    public void testEmptyMetadataWF(){
        String[] tag = {"kube_endpoint_labels", "namespace", "endpoint", "label_app", "label_app_kubernetes_io_managed_by", "label_component","label_heritage", "label_release", "label_service_kubernetes_io_headless"};
        List<List<String>> result = Parser.readFile(inputData, tag);

        assertEquals("kube_endpoint_labels", result.get(10).get(0));
        assertEquals("prometheus", result.get(10).get(2));
        assertEquals("prometheus-node-exporter", result.get(10).get(4));
        assertEquals("prometheus", result.get(10).get(6));
        assertEquals("Helm", result.get(10).get(8));
        assertEquals("node-exporter", result.get(10).get(10));
        assertEquals("Helm", result.get(10).get(12));
        assertEquals("prometheus", result.get(10).get(14));
        assertEquals("", result.get(10).get(16));
        assertEquals("1", result.get(10).get(17));
    }

    //Tests if the parser works correctly when metadata is empty witout filter
    @Test
    public void testEmptyMetadataNF(){
        String[] tag = {"kube_endpoint_labels"};
        List<List<String>> result = Parser.readFile(inputData, tag);

        assertEquals("kube_endpoint_labels", result.get(10).get(0));
        assertEquals("prometheus", result.get(10).get(2));
        assertEquals("prometheus-node-exporter", result.get(10).get(4));
        assertEquals("prometheus", result.get(10).get(6));
        assertEquals("Helm", result.get(10).get(8));
        assertEquals("node-exporter", result.get(10).get(12));
        assertEquals("Helm", result.get(10).get(14));
        assertEquals("prometheus", result.get(10).get(16));
        assertEquals("", result.get(10).get(18));
        assertEquals("1", result.get(10).get(19));
    }
}